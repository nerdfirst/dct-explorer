# About this Code

The code in this repository is part of a tutorial by **0612 TV** entitled **JPEG Encoding Magic**, hosted on YouTube.

[![Click to Watch](https://img.youtube.com/vi/MFvVqcl6JVs/0.jpg)](https://www.youtube.com/watch?v=MFvVqcl6JVs "Click to Watch")

If you wish to try this code, you can run it on the NERDfirst website: https://resources.nerdfirst.net/dct


# License

Note different parts of this project belong under different licenses. Please be aware of what license applies to which files.

## DCTjs Folder

In the **DCTjs** folder, the script named **dct.js** is a derivative work of the [GNU Classpath](https://www.gnu.org/software/classpath/) project, which is licensed under the GNU General Public license. As it is a derivative work, it is similarly licensed under the GNU GPL. For full license text, please refer to the COPYING and LICENSE files within the **DCTjs** folder.

## All other files

All files in this project, with the exception of the **DCTjs** folder, are licensed under the **Apache License 2.0**. For more details, please refer to LICENSE.txt within this repository.