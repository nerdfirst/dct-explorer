var DCTpanel;
var IMGpanel;

var DCTmatrix;
var IMGmatrix;

var DCTpanelComponents;
var IMGpanelComponents;

function begin() {
	prepVariables();
	gatherElements();
	buildGUI();
}

// -----------------

function prepVariables() {
	DCTmatrix = [];
	IMGmatrix = [];
	DCTpanelComponents = [];
	IMGpanelComponents = [];
	for (var i=0; i<8; i++) {
		DCTmatrix.push( [0,0,0,0,0,0,0,0] );
		IMGmatrix.push( [128,128,128,128,128,128,128,128] );
		DCTpanelComponents.push( [0,0,0,0,0,0,0,0] );
		IMGpanelComponents.push( [0,0,0,0,0,0,0,0] );
	}
}

function gatherElements() {
	DCTpanel = document.getElementById("dctPanel");
	IMGpanel = document.getElementById("imgPanel");
}

function buildGUI() {
	for (var i=0; i<8; i++) {
		for (var j=0; j<8; j++) {
			var elem = buildDCTPanel(i,j);
			DCTpanel.appendChild(elem);
			DCTpanelComponents[i][j] = elem;
		}
		var br = document.createElement("br");
		DCTpanel.appendChild(br);
	}
	
	for (var i=0; i<8; i++) {
		for (var j=0; j<8; j++) {
			var elem = buildIMGPanel(i,j);
			IMGpanel.appendChild(elem);
			IMGpanelComponents[i][j] = elem;
		}
		var br = document.createElement("br");
		IMGpanel.appendChild(br);
	}
}

// -----------------------

function buildDCTPanel(i,j) {
	var elem = document.createElement("div");
	elem.className = "panel";
	elem.i = i;
	elem.j = j;
	elem.innerHTML = DCTmatrix[i][j];
	
	elem.addEventListener("wheel", function(e) {
		e.preventDefault();
		var newValue = DCTmatrix[this.i][this.j] - e.deltaY*10;
		updateDCTvalue(this, newValue);
	});
	
	elem.addEventListener("mousedown", function(e) {
		e.preventDefault();
		switch(e.button) {
			case 0: updateDCTvalue(this, 1023); break;
			case 1: updateDCTvalue(this, 0); break;
			case 2: updateDCTvalue(this, -1024); break;
		}
	});
	
	elem.addEventListener("mousemove", function(e) {
		e.preventDefault();
		switch(e.buttons) {
			case 1: updateDCTvalue(this, 1024); break;
			case 4: updateDCTvalue(this, 0); break;
			case 2: updateDCTvalue(this, -1024); break;
		}
	});

	elem.addEventListener("contextmenu", function(e) {
		e.preventDefault();
	});
	
	elem.style.backgroundColor = "rgb(128,128,128)";
	
	return elem;
}

function buildIMGPanel(i,j) {
	var elem = document.createElement("div");
	elem.className = "panel";
	elem.i = i;
	elem.j = j;
	elem.innerHTML = IMGmatrix[i][j];
	
	elem.addEventListener("wheel", function(e) {
		e.preventDefault();
		var newValue = IMGmatrix[this.i][this.j] - e.deltaY*2;
		updateIMGvalue(this, newValue);
	});
	
	elem.addEventListener("mousedown", function(e) {
		e.preventDefault();
		switch(e.button) {
			case 0: updateIMGvalue(this, 255); break;
			case 1: updateIMGvalue(this, 128); break;
			case 2: updateIMGvalue(this, 0); break;
		}
	});
	
	elem.addEventListener("mousemove", function(e) {
		e.preventDefault();
		switch(e.buttons) {
			case 1: updateIMGvalue(this, 255); break;
			case 4: updateIMGvalue(this, 128); break;
			case 2: updateIMGvalue(this, 0); break;
		}
	});

	elem.addEventListener("contextmenu", function(e) {
		e.preventDefault();
	});
	
	elem.style.backgroundColor = "rgb(128,128,128)";
	
	return elem;
}

// -----------------

function updateDCTvalue(obj,val) {
	val = Math.max(-1024, Math.min(val, 1023));
	DCTmatrix[obj.i][obj.j] = val;
	obj.innerHTML = val;
	var normalized = Math.round(((val+1024) / 2047.0) * 255);
	obj.style.backgroundColor = "rgb(" + normalized + "," + normalized + "," + normalized + ")";
	
	updateIMGmatrix();
}

function updateIMGvalue(obj,val) {
	val = Math.max(0, Math.min(val, 255));;
	IMGmatrix[obj.i][obj.j] = val;
	obj.innerHTML = val;
	obj.style.backgroundColor = "rgb(" + val + "," + val + "," + val + ")";
	
	updateDCTmatrix();
}

// -------------------

function updateIMGmatrix() {
	IMGmatrix = inverseDCT(DCTmatrix);
	for (var i=0; i<8; i++) {
		for (var j=0; j<8; j++) {
			var obj = IMGpanelComponents[i][j];
			var val = IMGmatrix[i][j];
			obj.innerHTML = val;
			obj.style.backgroundColor = "rgb(" + val + "," + val + "," + val + ")";
		}
	}
}

function updateDCTmatrix() {
	DCTmatrix = forwardDCT(IMGmatrix);
	for (var i=0; i<8; i++) {
		for (var j=0; j<8; j++) {
			var obj = DCTpanelComponents[i][j];
			var val = DCTmatrix[i][j];
			var normalized = Math.round(((val+1024) / 2047.0) * 255);
			obj.innerHTML = val;
			obj.style.backgroundColor = "rgb(" + normalized + "," + normalized + "," + normalized + ")";
		}
	}
}